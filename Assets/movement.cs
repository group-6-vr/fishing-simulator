﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class movement : MonoBehaviour
{
    float value = 0;
    public GameObject reel;
    Quaternion prevRotation;
    // Start is called before the first frame update
    void Start()
    {
        prevRotation = reel.transform.localRotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(prevRotation != reel.transform.localRotation)
        {
            value += Quaternion.Angle(prevRotation, reel.transform.localRotation);
        }
        gameObject.GetComponent<TextMeshPro>().text = "" + value;
        prevRotation = reel.transform.localRotation;
    }
    public void addOne(float input)
    {
        value+=input;
    }
}
