﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoolerScript : MonoBehaviour
{
    public GameObject score;
    public GameObject fishDisplay;
    private int scorenum;
    private int weight;
    private int age;
    private string fishInfo;
    public AudioSource fishThud;
    // Start is called before the first frame update
    void Start()
    {

        fishInfo = "";
        weight = 0;
        age = 0;
        scorenum = 0;
        score.GetComponent<TextMeshPro>().text = "" + scorenum;
        fishDisplay.GetComponent<TextMeshPro>().text = fishInfo;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        fishInfo = "";
        if (other.gameObject.name == "Beta Fish")
        {
            weight = Random.Range(1, 3);
            age = Random.Range(2, 6);
            fishInfo = "Fish Type: Beta Fish \n Weight: " + weight +
                        "\nAge: " + age +
                        "\nLives in 75 - 80 degree shallow water with thick vegetation";
        }
        else if (other.gameObject.name == "Brown Trout")
        {
            weight = Random.Range(5, 45);
            age = Random.Range(2, 10);
            fishInfo = "Fish Type: Brown Trout \n Weight: " + weight +
                        "\nAge: " + age +
                        "\nLives in 45 - 65 degree cold water streams";
        }
        else if (other.gameObject.name == "Carp")
        {
            weight = Random.Range(5, 30);
            age = Random.Range(2, 20);
            fishInfo = "Fish Type: Carp \n Weight: " + weight +
                        "\nAge: " + age +
                        "\nLives in 70 - 85 degree fresh water";
        }
        else if (other.gameObject.name == "Rudd Fish")
        {
            weight = Random.Range(1, 3);
            age = Random.Range(2, 17);
            fishInfo = "Fish Type: Rudd \n Weight: " + weight +
                        "\nAge: " + age +
                        "\nLives in 50 - 70 clear water with vegetation";
        }
        else if (other.gameObject.name == "Sunfish")
        {
            weight = Random.Range(1, 3);
            age = Random.Range(2, 8);
            fishInfo = "Fish Type: Sunfish \n Weight: " + weight +
                        "\nAge: " + age +
                        "\nLives in 65 - 80 degree shallow water with vegetation";
        }
        else if (other.gameObject.name == "Yellow Snapper")
        {
            weight = Random.Range(1, 5);
            age = Random.Range(2, 14);
            fishInfo = "Fish Type: Yellow Snapper \n Weight: " + weight +
                        "\nAge: " + age +
                        "\nLives in 50 - 70 degree sandy areas near reefs";
        }

        if (other.gameObject.tag == "Fish")
        {
            fishThud.Play(0);
            Destroy(other.gameObject);
            scorenum += 1;
            score.GetComponent<TextMeshPro>().text = "" + scorenum;
            GameObject bobber = GameObject.FindWithTag("Bobber");
            bobber.GetComponent<joint>().hasFish = false;
            fishDisplay.GetComponent<TextMeshPro>().text = fishInfo;
        }
        if (other.gameObject.tag == "leftcontroller")
        {
            scorenum = 0;
            score.GetComponent<TextMeshPro>().text = "" + scorenum;
            fishDisplay.GetComponent<TextMeshPro>().text = "";
        }
    }
    /*private void OnCollisionEnter(Collider other)
    {
        if (other.gameObject.layer == 12)
        {
            Debug.Log("I have detected collision 2");
            Destroy(other.gameObject.transform.parent);
            scorenum += 1;
            score.GetComponent<TextMeshPro>().text = "" + scorenum;
        }
    }*/
}
