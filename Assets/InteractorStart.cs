﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractorStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /*private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "hook")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<BoxCollider>());
        }
        if (collision.gameObject.tag == "Water")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<BoxCollider>());
        }
    }*/
    public void UnParent()
    {
        gameObject.transform.SetParent(null);
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        
    }
    public void updateBobberOnFishState()
    {
        if (gameObject.transform.parent != null) { 
            GameObject bobber = GameObject.FindWithTag("Bobber");
            bobber.GetComponent<joint>().hasFish = false;
            gameObject.transform.GetChild(0).GetChild(0).GetChild(0).transform.localPosition = Vector3.zero;
        }
    }
}
