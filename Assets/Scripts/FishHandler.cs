﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishHandler : MonoBehaviour
{
    GameObject bobber;
    // Start is called before the first frame update
    void Start()
    {
        bobber = GameObject.FindWithTag("Bobber");
        gameObject.GetComponent<BoxCollider>().enabled = true;
        //gameObject.transform.localPosition = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        /*if (OVRInput.GetDown(OVRInput.Button.Two, OVRInput.Controller.RTouch))
        {
            gameObject.transform.parent = null;
            //gameObject.GetComponent<Rigidbody>().isKinematic = false;
            gameObject.GetComponent<BoxCollider>().enabled = true;
            bobber.GetComponent<joint>().hasFish = false;
        }*/
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "hook")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<BoxCollider>());
        }
        if (collision.gameObject.tag == "Water")
        {
            Physics.IgnoreCollision(collision.collider, gameObject.GetComponent<BoxCollider>());
        }
    }
}
