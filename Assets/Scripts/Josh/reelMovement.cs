﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reelMovement : MonoBehaviour
{
    public float speed;
    public GameObject Bobber;
    public double newDistance;
    public GameObject anchor;
    public float bobberForce;
    bool pressingReelIn = false;
    public GameObject reel;
    bool reelGrabbed;
    Quaternion prevRotation;
    float hapticController = 0;
    public AudioSource reelSound;
    // Start is called before the first frame update
    void Start()
    {
        Bobber.AddComponent<SpringJoint>();
        Bobber.GetComponent<SpringJoint>().autoConfigureConnectedAnchor = false;
        Bobber.GetComponent<SpringJoint>().connectedBody = anchor.GetComponent<Rigidbody>();
        Bobber.GetComponent<SpringJoint>().connectedAnchor = Vector3.zero;

        Bobber.GetComponent<SpringJoint>().spring = 10;
        Bobber.GetComponent<SpringJoint>().damper = 0.2f;
        Bobber.GetComponent<SpringJoint>().maxDistance = Vector3.Distance(Bobber.transform.position, anchor.transform.position);
        prevRotation = reel.transform.localRotation;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        /* bobberForce = Bobber.GetComponent<Rigidbody>().velocity.magnitude;
         if (Input.GetKey(KeyCode.F) || pressingReelIn)
         {
             Bobber.GetComponent<joint>().touchingWater = false;
             Bobber.GetComponent<Rigidbody>().isKinematic = false;
             Bobber.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
             Bobber.GetComponent<SphereCollider>().isTrigger = false;
             Bobber.GetComponent<joint>().activated = false;
             if (Bobber.GetComponent<SpringJoint>() == null)
             {
                 Bobber.AddComponent<SpringJoint>();
                 Bobber.GetComponent<SpringJoint>().autoConfigureConnectedAnchor = false;
                 Bobber.GetComponent<SpringJoint>().connectedBody = anchor.GetComponent<Rigidbody>();
                 Bobber.GetComponent<SpringJoint>().connectedAnchor = Vector3.zero;

                 Bobber.GetComponent<SpringJoint>().spring = 10;
                 Bobber.GetComponent<SpringJoint>().damper = 0.2f;
                 Bobber.GetComponent<SpringJoint>().maxDistance = Vector3.Distance(Bobber.transform.position, anchor.transform.position);
             }
             gameObject.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(0, 1 * speed, 0));
             if(Bobber.GetComponent<SpringJoint>().maxDistance - speed * 0.1f * Time.deltaTime > 0.001f)
             Bobber.GetComponent<SpringJoint>().maxDistance -= speed * 0.1f * Time.deltaTime;
         }*/

        if (prevRotation != reel.transform.localRotation && reelGrabbed)
        {
            Bobber.GetComponent<joint>().touchingWater = false;
            Bobber.GetComponent<Rigidbody>().isKinematic = false;
            Bobber.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            Bobber.GetComponent<SphereCollider>().isTrigger = false;
            Bobber.GetComponent<joint>().activated = false;
            if (Bobber.GetComponent<SpringJoint>() == null)
            {
                Bobber.AddComponent<SpringJoint>();
                Bobber.GetComponent<SpringJoint>().autoConfigureConnectedAnchor = false;
                Bobber.GetComponent<SpringJoint>().connectedBody = anchor.GetComponent<Rigidbody>();
                Bobber.GetComponent<SpringJoint>().connectedAnchor = Vector3.zero;

                Bobber.GetComponent<SpringJoint>().spring = 10;
                Bobber.GetComponent<SpringJoint>().damper = 0.2f;
                Bobber.GetComponent<SpringJoint>().maxDistance = Vector3.Distance(Bobber.transform.position, anchor.transform.position);
            }
            Bobber.GetComponent<SpringJoint>().maxDistance -= 0.01f * Quaternion.Angle(prevRotation, reel.transform.localRotation);
            hapticController += 0.01f * Quaternion.Angle(prevRotation, reel.transform.localRotation);
            if(hapticController >= 0.07f)
            {
                reelSound.Play(0);
                OVRInput.SetControllerVibration(0.8f, 0.8f, OVRInput.Controller.LTouch);
                StartCoroutine(ExecuteAfterTime(0.1f));
                hapticController = 0;
            }
        }
        prevRotation = reel.transform.localRotation;
    }
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.LTouch);

        // Code to execute after the delay
    }
    public void Grabbed()
    {
        reelGrabbed = true;
    }
    public void Released()
    {
        reelGrabbed = false;
    }
    public void pressingReel()
    {
        pressingReelIn = true;
    }
    public void releasingReel()
    {
        pressingReelIn = false;
    }
}
