﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class RenderLine : MonoBehaviour
{
    public GameObject g1;
    public GameObject g2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.GetComponent<LineRenderer>().SetPosition(0, g1.transform.position);
        gameObject.GetComponent<LineRenderer>().SetPosition(1, g2.transform.position);
    }
}
