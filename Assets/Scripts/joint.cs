﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class joint : MonoBehaviour
{
    //public GameObject hingeObject;
    public float velocity;
    public float speed;
    public GameObject spawner;
    public bool hasFish = false;
    public Rigidbody rgbody;
    public bool touchingWater = false;
    Vector3 carryOverVelocity;
    public Animator anim;
    public GameObject waterSplash;
    public GameObject water;
    public bool activated = false;
    public AudioSource castSound;
    public AudioSource splashSound;
    // Start is called before the first frame update
    void Start()
    {
        
        // Given:

    }

    // Update is called once per frame
    void Update()
    {
        //gameObject.GetComponent<SpringJoint>().connectedAnchor = hingeObject.transform.position;
        velocity = gameObject.GetComponent<Rigidbody>().velocity.magnitude;
        if (Input.GetKey(KeyCode.G) && gameObject.GetComponent<SpringJoint>() != null)
        {
           //gameObject.GetComponent<SpringJoint>().maxDistance += speed * 0.1f * Time.deltaTime;
           gameObject.GetComponent<SpringJoint>().breakForce = 14;
        }
        if (Input.GetKeyUp(KeyCode.G) && gameObject.GetComponent<SpringJoint>() != null)
        {
           gameObject.GetComponent<SpringJoint>().breakForce = Mathf.Infinity;
        }
        /*if (activated && gameObject.GetComponent<SpringJoint>() != null)
        {
            gameObject.GetComponent<SpringJoint>().breakForce = 14;
        }
        else if(!activated && gameObject.GetComponent<SpringJoint>() != null)
        {
            gameObject.GetComponent<SpringJoint>().breakForce = Mathf.Infinity;
        }*/
        //if(disableRig )
        //{
        //    rgbody.isKinematic = true;
        //    gameObject.GetComponent<SphereCollider>().isTrigger = true;
        //}
        if (touchingWater)
        {
            anim.SetBool("lightBob", true);
            Debug.Log("I caught something!");
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(-90, 0, -30), 1f * Time.deltaTime);
            if (hasFish)
            {
                anim.SetBool("Bobbing", true);
            }
        }
        else
        {
            anim.SetBool("Bobbing", false);
            anim.SetBool("lightBob", false);
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        castSound.Stop();
        if (collision.gameObject.tag == "Water")
        {
            splashSound.Play(0);
        }
        if(collision.gameObject.tag == "Water" && !hasFish)
        {
            spawner.GetComponent<spawner>().bobberHits();
        }
        if (collision.gameObject.tag == "Water")
        {
            OVRInput.SetControllerVibration(0, 0, OVRInput.Controller.RTouch);
            rgbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            touchingWater = true;
            Instantiate(waterSplash, new Vector3(transform.position.x, water.transform.position.y + 0.1f, transform.position.z), Quaternion.Euler(-90,-90,0));
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Water" && !hasFish)
        {
            spawner.GetComponent<spawner>().isActive = false;
        }
        if (collision.gameObject.tag == "Water")
        {
            rgbody.constraints = RigidbodyConstraints.None;
            touchingWater = false;
        }
    }
    public void controllerPressed()
    {
        if(gameObject.GetComponent<SpringJoint>() != null)
        {
            //gameObject.GetComponent<SpringJoint>().breakForce = 14;
            activated = true;
        }
    }
    public void controllerReleased()
    {
        if (gameObject.GetComponent<SpringJoint>() != null && activated)
        {
            Destroy(gameObject.GetComponent<SpringJoint>());
            //gameObject.GetComponent<SpringJoint>().breakForce = Mathf.Infinity;
            activated = false;
            OVRInput.SetControllerVibration(1f, 1f, OVRInput.Controller.RTouch);
            StartCoroutine(ExecuteAfterTime(0.1f));
            castSound.Play(0);
        }
    }
    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        OVRInput.SetControllerVibration(0.3f, 0.3f, OVRInput.Controller.RTouch);

        // Code to execute after the delay
    }
}
