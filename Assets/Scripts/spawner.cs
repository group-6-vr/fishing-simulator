using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class spawner : MonoBehaviour
{
    public GameObject[] fishPrefabs;
    //public GameObject spawnee;
    public GameObject bobber;
    public GameObject hook;
    public bool isActive = false;
    public GameObject interactable;
    // Calls spawnFish between 0 and 20 seconds
    // should recieve bobber position and can replace what is currently "spawnPos"
    public void bobberHits()
    {
        if (!isActive) {
            var number = Random.value;
            number *= 20;
            Invoke("spawnFish", number);
        }
        isActive = true;
    }

    // creates Nemo at the provided spawn location
    // spawn location should change to where the bobber hits the water
    public void spawnFish() {
        if (isActive)
        {
            int r = Random.Range(0, fishPrefabs.Length);
            GameObject interact = Instantiate(interactable, hook.transform.position, hook.transform.rotation);
            interact.transform.SetParent(hook.transform);
            interact.transform.localPosition = Vector3.zero;
            interact.transform.Rotate(new Vector3(90, 0, 0));
            GameObject meshContainer = interact.transform.Find("MeshContainer").gameObject;
            GameObject fish = Instantiate(fishPrefabs[r], hook.transform.position, hook.transform.rotation);
            fish.transform.SetParent(meshContainer.transform);
            fish.transform.localPosition = Vector3.zero;
            fish.transform.Rotate(new Vector3(90, 0, 0));
            fish.transform.localScale = new Vector3(0.006f, 0.006f, 0.006f);
            isActive = false;
            Debug.Log("Spawned Fish");
            bobber.GetComponent<joint>().hasFish = true;
        }
    }
}