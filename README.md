# Fishing Simulator
A virtual reality fishing game that runs natively on the oculus quest.
## - About the Project -
This was a semester-long project that was worked on by me (Joshua Williams), Anthony Frazier, Jack Maggard, and Andrew Wolstenholme for Virtual Reality Systems.  We were told essentially to just develop an application for Virtual Reality and we came up with the idea to create a fishing simulation.  This project runs natively on the oculus quest and utilizes the quest's touch controllers for immersive motion controls and haptic feedback.  The fishing rod can be cast by doing an intuitive motion gesture and it can also be reeled in by simply mimicking the motion of reeling in a fishing reel.  A random selection of fish can be caught and you can recieve information about the fish by placing them into the cooler located on the pier.  
## - Built With -
* [Unity](https://unity.com/)
* [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)
* [VRTK (Virtual Realtiy Toolkit)](https://github.com/ExtendRealityLtd/VRTK.Prefabs)
## - Installation -
* Download the fishing-simulator.apk located in the release folder
* Install [Sidequest](https://sidequestvr.com/)
* Plug in Oculus Quest headset and install the apk onto the device (You may have to enable developer mode on your quest first)
* Put on Quest and navigate to unknown sources in apps, and run the application
## - Usage - 
* Use the grab button on the controllers to pick up objects
* Use the right trigger to cast the fishing rod
* Grab the reel with your left controller and reel in the fishing rod to catch fish
